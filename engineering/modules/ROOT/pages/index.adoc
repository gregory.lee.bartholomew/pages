= Engineering Teams
:page-layout: without_menu

== Core Teams

* xref:fesco:ROOT:index.adoc[**Fedora Engineering Steering Committee**] — FESCo is the Fedora Engineering Steering Committee. It is a fully community elected body and represents the technical leadership in Fedora.

* xref:package-maintainers:ROOT:index.adoc[**Fedora Package Maintainers**] — Documentation for community members involved in Fedora package maintenance.

* xref:infra:ROOT:index.adoc[**Fedora Infrastructure**] — Everything you need to know about Fedora's infrastructure and the people running it.

* xref:qa-docs:ROOT:index.adoc[**Fedora QA**] — This docs outlines all the activities you can get involved in to help with Fedora QA (Quality Assurance) and is meant to guide you through the QA ecosystem.

* xref:cpe:ROOT:index.adoc[**CPE**] - The Community Platform Engineering Team is a Red Hat team dedicated to the Fedora and CentOS projects where they contribute to the infrastructure and release engineering.

== Fedora Editions

* xref:workstation-working-group:ROOT:index.adoc[**Workstation Working Group**] — Information about the group behind Fedora Workstation
* xref:server-working-group::index.adoc[**Server Working Group**] - Information about the work on Fedora Server Edition
* xref:iot:ROOT:index.adoc[**IoT**] — Fedora's Internet of Things platform.

== Initiatives & Special Interest Teams

* xref:modularity:ROOT:index.adoc[**Fedora Modularity**] — Modularity introduces an optional Modular repository that provides additional versions of software on independent lifecycles.

* xref:containers:ROOT:index.adoc[**Fedora Containers**] — Here you will find Documentation and Guidelines regarding creation, usage and maintainance of Containers in Fedora.

* xref:neurofedora:ROOT:overview.adoc[**NeuroFedora**] — NeuroFedora is an initiative to provide a ready to use Fedora based Free/Open source software platform for neuroscience.

* xref:teleirc-sig:ROOT:index.adoc[**Fedora Teleirc SIG**] — The Teleirc SIG is a volunteer-driven group to manage Teleirc bridge bots.

* xref:ci:ROOT:index.adoc[**Fedora CI**] — Fedora Continuous Integration Portal

* xref:eln:ROOT:index.adoc[**Fedora ELN**] — Buildroot and compose process using Rawhide to emulate https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux[Red Hat Enterprise Linux]

* xref:minimization:ROOT:index.adoc[**Fedora Minimization Objective**] — Home of the Fedora Minimization Objective.

* xref:i3:ROOT:index.adoc[**i3 SIG**] - Documentation for the Fedora Special Interest Group centered about the **i3wm** window manager.

* xref:gaming:ROOT:index.adoc[**Fedora Gaming**] — Learn all about gaming and game development on Fedora.

== Other Engineering Resources

* xref:flatpak:ROOT:index.adoc[**Flatpak in Fedora**] — Learn how to build Flatpaks out of Fedora RPM packages.

* xref:fedora-accounts:ROOT:index.adoc[**Fedora Accounts**] — Learn Fedora Accounts, the authentication system used throughout the Fedora Project.

* xref:releases:ROOT:index.adoc[**Releases**] — Information about release planning and processes.

* xref:defensive-coding:ROOT:index.adoc[**Defensive Coding Guide**] — Guidelines for improving software security in Fedora through secure coding.